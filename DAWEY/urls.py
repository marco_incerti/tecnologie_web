"""DAWEY URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from DAWEY.views import HomePageView, HomePageView_Seminario_User, HomePageView_User, HomePageView_Corso_User, \
    HomePageView_Competizione_User, HomePageView_Sportivo_User

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('home', HomePageView_User.as_view(), name='home_user'),
    path('home/seminario', HomePageView_Seminario_User.as_view(), name='home_user_seminario'),
    path('home/corso', HomePageView_Corso_User.as_view(), name='home_user_corso'),
    path('home/competizione', HomePageView_Competizione_User.as_view(), name='home_user_competizione'),
    path('home/sportivo', HomePageView_Sportivo_User.as_view(), name='home_user_sportivo'),
    path('user_management/', include('user_management.urls')),
    path('eventi_management/', include('eventi_management.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
