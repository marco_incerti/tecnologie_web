from datetime import date
from itertools import chain

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from eventi_management.models import Category, Tags, Seminario, Event, Corso, Competizione, Sportivo
from user_management.models import UserAndEvent


class BasicView:
    """
    View for getting the categories from the db.

    :param categories: the query set that contains all the categories of the app
    """
    categories = Category.objects.all()
    tags = Tags.objects.all()


class HomePageView(BasicView, TemplateView):
    """
    Home view of the application
    """
    template_name = 'homepage.html'
    object_users = None
    object_event = None
    object_userandevents = None
    context_object_name = 'object_list'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)

        self.object_users = User.objects.all().exclude(is_superuser=True)  # Gets all users
        self.object_event = Event.objects.all()  # Gets all event
        self.object_userandevents = UserAndEvent.objects.all()

        return context


class HP_base(BasicView, TemplateView):
    """
    richiede tutti i dati alle varie tabelle, poi le unisce in una lista
    """
    template_name = 'homepage_user.html'
    object_list = None
    object_eventi_prenotati = None
    seminario = None
    corso = None
    competizione = None
    sportivo = None

    def get_context_data(self, **kwargs):
        context = super(HP_base, self).get_context_data(**kwargs)
        id_eventi_prenotati = UserAndEvent.objects.filter(user=self.request.user.pk).values('id_event')

        if self.request.user.is_authenticated:
            self.object_eventi_prenotati = Event.objects.filter(date_start__gte=date.today(),
                                                                pk__in=id_eventi_prenotati)

        self.seminario = Seminario.objects.filter(date_start__gte=date.today()).exclude(
            pk__in=id_eventi_prenotati)
        self.corso = Corso.objects.filter(date_start__gte=date.today()).exclude(
            pk__in=id_eventi_prenotati)
        self.competizione = Competizione.objects.filter(date_start__gte=date.today()).exclude(
            pk__in=id_eventi_prenotati)
        self.sportivo = Sportivo.objects.filter(date_start__gte=date.today()).exclude(
            pk__in=id_eventi_prenotati)

        return context


class HomePageView_User(HP_base):

    def get_context_data(self, **kwargs):
        context = super(HomePageView_User, self).get_context_data(**kwargs)

        self.object_list = list(chain(self.seminario, self.corso, self.competizione, self.sportivo))

        return context


class HomePageView_Seminario_User(LoginRequiredMixin, HP_base):

    def get_context_data(self, **kwargs):
        context = super(HomePageView_Seminario_User, self).get_context_data(**kwargs)

        self.object_list = self.seminario

        return context


class HomePageView_Corso_User(LoginRequiredMixin, HP_base):

    def get_context_data(self, **kwargs):
        context = super(HomePageView_Corso_User, self).get_context_data(**kwargs)

        self.object_list = self.corso
        return context


class HomePageView_Competizione_User(LoginRequiredMixin, HP_base):

    def get_context_data(self, **kwargs):
        context = super(HomePageView_Competizione_User, self).get_context_data(**kwargs)

        self.object_list = self.competizione

        return context


class HomePageView_Sportivo_User(LoginRequiredMixin, HP_base):

    def get_context_data(self, **kwargs):
        context = super(HomePageView_Sportivo_User, self).get_context_data(**kwargs)

        self.object_list = self.sportivo

        return context
