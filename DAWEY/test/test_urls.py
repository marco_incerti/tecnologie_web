from django.test import SimpleTestCase
from django.urls import reverse, resolve

from DAWEY.views import HomePageView_User


class TestUrls(SimpleTestCase):
    """Test class to check that base urls retrieve the correct django template"""
    def test_home_is_resolved(self):
        url = reverse("home_user")
        self.assertEquals(resolve(url).func.view_class, HomePageView_User)
