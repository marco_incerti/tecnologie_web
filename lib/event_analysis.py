from eventi_management.models import Seminario, Event
from user_management.models import UserAndEvent


def contare_partecipanti_evento(evento):
    n_partecipanti = UserAndEvent.objects.filter(id_event=evento).count()
    return n_partecipanti


def differenza_partecipanti_evento(evento):
    n_partecipanti = UserAndEvent.objects.filter(id_event=evento.pk).count()
    n_disponibili = evento.posti - n_partecipanti
    return n_disponibili


def get_affluenza(evento):
    n_partecipanti = UserAndEvent.objects.filter(id_event=evento.pk).count()
    posti_disponibili = Event.objects.get(pk=evento.pk)

    if posti_disponibili.posti is not None:
        affluenza = 0 if n_partecipanti == 0 else n_partecipanti / posti_disponibili.posti
    else:
        affluenza = 0 if n_partecipanti == 0 else n_partecipanti / n_partecipanti

    return affluenza


def get_id_UserEvent(value, arg):
    id_evento_user = UserAndEvent.objects.get(id_event=arg, user_id=value)

    return id_evento_user.pk