from datetime import date
from itertools import chain

from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.contrib.auth.forms import PasswordResetForm
from django.template.loader import render_to_string
from django.db.models.query_utils import Q
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, TemplateView, FormView

from DAWEY.views import BasicView
from eventi_management.models import Seminario, Competizione, Sportivo, Corso
from user_management.forms import RegistrationForm, LoginForm, UpdateUserForm
from user_management.models import UserAndEvent


class CreateUserView(CreateView):
    """
    View with registration form
    """
    form_class = RegistrationForm
    template_name = 'registration/registration_form.html'
    success_url = reverse_lazy('user_management:user-login')


class LoginUserView(LoginView):
    """
    View with login form
    """
    form_class = LoginForm
    template_name = 'registration/login.html'
    success_url = reverse_lazy('home')


class PasswordResetUserView(PasswordResetView):
    """
    View with login form
    """
    form_class = LoginForm
    template_name = 'registration/login.html'
    success_url = reverse_lazy('home')


class LogoutUserView(LogoutView):
    """
    View for logging out
    """
    template_name = 'registration/logged_out.html'
    success_url = reverse_lazy('user_management:user-login')


class ProfileView(BasicView, TemplateView, LoginRequiredMixin):
    """
    View for displaying the account of the authenticated user.
    """
    template_name = 'user_profile/user_profile.html'
    user_for_profile = None
    events = None
    seminario = None
    corso = None
    competizione = None
    sportivo = None

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)

        try:
            self.user_for_profile = get_object_or_404(User, pk=self.request.user.pk)

            id_eventi_prenotati = UserAndEvent.objects.filter(user=self.request.user.pk).values('id_event')
            self.seminario = Seminario.objects.filter(date_start__lte=date.today(), pk__in=id_eventi_prenotati)
            self.corso = Corso.objects.filter(date_start__lte=date.today(), pk__in=id_eventi_prenotati)
            self.competizione = Competizione.objects.filter(date_start__lte=date.today(), pk__in=id_eventi_prenotati)
            self.sportivo = Sportivo.objects.filter(date_start__lte=date.today(), pk__in=id_eventi_prenotati)
            self.events = list(chain(self.seminario, self.corso, self.competizione, self.sportivo))
        except ObjectDoesNotExist:
            raise Http404

        return context


class UpdateUserView(BasicView, FormView, LoginRequiredMixin):
    """
    View for updating the data of the authenticated user account.
    """
    template_name = 'user_profile/user_update.html'
    success_url = reverse_lazy('user_management:user')

    def post(self, request, *args, **kwargs):
        # Sends the authenticated user to the form in order to update it.
        form = UpdateUserForm(request.POST, request.user)
        # Click on "Cancel" button
        if "cancel" in request.POST:
            return HttpResponseRedirect(reverse_lazy('user_management:user'))
        # Click on "Submit" button
        else:
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse_lazy('user_management:user'))

            context = ({'view': self,
                        'form': form,
                        })
            return super(UpdateUserView, self).render_to_response(context)

    def get(self, request, *args, **kwargs):
        # Populates the form with the authenticated user data.
        form = UpdateUserForm(initial={
            'username': request.user.username,
            'email': request.user.email,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
        })
        context = ({'view': self,
                    'form': form,
                    })
        return super(UpdateUserView, self).render_to_response(context)


class DeleteUserView(LoginRequiredMixin, View):
    """
    View for deleting the account of the authenticated user.
    Redirects to :view: LoginUserView
    """

    def get(self, request):
        User.objects.get(pk=request.user.pk).delete()
        return HttpResponseRedirect(reverse_lazy('user_management:user-login'))

    def post(self):
        User.objects.get(pk=self.request.user.pk).delete()
        return HttpResponseRedirect(reverse_lazy('user_management:user-login'))


@login_required
@require_POST
@csrf_protect
def update_user_responsabile(request):
    if request.method == "POST":
        utente = request.user
        if utente.user_settings.is_prof:
            utente.user_settings.is_prof = False
        else:
            utente.user_settings.is_prof = True

        utente.user_settings.save()
        data = {
            'Aggiornato': True
        }
        return JsonResponse(data)


def password_reset_request(request):
    if request.method == "POST":
        password_reset_form = PasswordResetForm(request.POST)
        if password_reset_form.is_valid():
            data = password_reset_form.cleaned_data['email']
            associated_users = User.objects.filter(Q(email=data))
            if associated_users.exists():
                for user in associated_users:
                    subject = "Password Reset Requested"
                    email_template_name = "password_reset_email.txt"
                    c = {
                        "email": user.email,
                        'domain': '127.0.0.1:8000',
                        'site_name': 'Website',
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "user": user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                    }
                    email = render_to_string(email_template_name, c)
                    try:
                        send_mail(subject, email, 'incerti.marco1997@gmail.com', [user.email], fail_silently=False)
                    except BadHeaderError:
                        return HttpResponse('Invalid header found.')
                    return redirect("user_management:password_reset_done")

            else:
                messages.error(request, 'Non esistono utenti con questa email.')
    password_reset_form = PasswordResetForm()
    return render(request=request, template_name="registration/reset_password.html",
                  context={"password_reset_form": password_reset_form})
