from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from eventi_management.models import Seminario, Event, Universita, Corso


class UserSettings(models.Model):

    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE, related_name='user_settings')
    is_prof = models.BooleanField(default=False)
    universita = models.ForeignKey(Universita, on_delete=models.SET_NULL, null=True, blank=True)
    corso = models.ForeignKey(Corso, on_delete=models.SET_NULL, null=True, blank=True)

    @receiver(post_save, sender=User)
    def create_favorites(sender, instance, created, **kwargs):
        if created:
            UserSettings.objects.create(user=instance)


class UserAndEvent(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    id_event = models.ForeignKey(Event, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
