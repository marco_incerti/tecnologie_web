from django.urls import path, reverse_lazy
from user_management import views
from django.contrib.auth import views as auth_views

app_name = 'user_management'

urlpatterns = [
    path('password_reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='registration/reset_password_done.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="registration/reset_password_confirm.html",
                                                     success_url=reverse_lazy('user_management:password_reset_complete')),
         name='password_reset_confirm'),
    path('reset/done/',
         auth_views.PasswordResetCompleteView.as_view(template_name='registration/reset_password_complete.html'),
         name='password_reset_complete'),
    path("password_reset", views.password_reset_request, name="password_reset"),

    path('registration', views.CreateUserView.as_view(), name='user-register'),
    path('login', views.LoginUserView.as_view(), name='user-login'),
    path('logout', views.LogoutUserView.as_view(), name='user-logout'),
    path('profile', views.ProfileView.as_view(), name='user'),
    path('profile/update', views.UpdateUserView.as_view(), name='user-update'),
    path('update-user-responsabile', views.update_user_responsabile, name='user-update-ajax'),
    path('profile/delete', views.DeleteUserView.as_view(), name='delete-user'),
]
