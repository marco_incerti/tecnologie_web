from django.contrib import admin

# Register your models here.
from user_management.models import UserSettings, UserAndEvent

admin.site.register(UserSettings)
admin.site.register(UserAndEvent)