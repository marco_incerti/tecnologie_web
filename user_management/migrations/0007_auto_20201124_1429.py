# Generated by Django 3.1.1 on 2020-11-24 14:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_management', '0006_auto_20201124_1358'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userandevents',
            old_name='id_event',
            new_name='event',
        ),
    ]
