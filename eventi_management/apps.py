from django.apps import AppConfig


class EventiManagementConfig(AppConfig):
    name = 'eventi_management'
