from lib import event_analysis
from django.template.defaulttags import register


@register.filter
def get_n_partecipanti(event):
    return event_analysis.contare_partecipanti_evento(event)


@register.filter
def get_differenze_n_partecipanti(event):
    return event_analysis.differenza_partecipanti_evento(event)


@register.filter
def get_evento_affluenza(evento):
    """
    :param evento:
    :return: un double che rispecchia l'affluenza
    """
    return event_analysis.get_affluenza(evento)


@register.filter(is_safe=True)
def get_id_UserEvent(value, arg):
    return event_analysis.get_id_UserEvent(value, arg)


@register.filter(name='get_class')
def get_class(value):
    return value.__class__.__name__
