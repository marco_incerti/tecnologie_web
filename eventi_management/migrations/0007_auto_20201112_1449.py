# Generated by Django 3.1.1 on 2020-11-12 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eventi_management', '0006_auto_20201112_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='foto',
            field=models.ImageField(blank=True, error_messages={'invalid': 'Image files only'}, null=True, upload_to='images/'),
        ),
    ]
