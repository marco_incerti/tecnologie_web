from django.urls import reverse
from django.test import Client, TestCase

from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from eventi_management.models import Event, Category


class TestViewsEventManager(TestCase):

    def setUp(self):

        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.evento = Event.objects.create(
            user_owner=self.user,
            title="Promessi Sposi",
            category_id=Category.objects.last(),
            date='2021-06-06 09:00:00',
            descrizione="ciao evento di prova",

        )

    def testLogin(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(reverse('user_management:user-login'))
        self.assertEqual(response.status_code, 200)

    def test_view_event_insert_GET(self):
        response = self.client.get(reverse('eventi_management:corso-insert'))
        self.assertEquals(response.status_code, 302)
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(reverse('eventi_management:corso-insert'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'inserimeto_evento/inserimento_form.html')