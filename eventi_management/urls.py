from django.urls import path

from eventi_management import views

app_name = 'eventi_management'

urlpatterns = [
    path('event/insert', views.InsertEvent.as_view(), name='event-insert'),
    path('event/insert/seminario', views.SeminarioInsert.as_view(), name='seminario-insert'),
    path('event/insert/competizione', views.CompetizioneEvent.as_view(), name='competizione-insert'),
    path('event/insert/corso', views.CorsoInsert.as_view(), name='corso-insert'),
    path('event/insert/sportivo', views.SportivoInsert.as_view(), name='sportivo-insert'),
    path('event/list', views.EventListUserCreate.as_view(), name='event-list'),
    path('event/<int:pk>', views.EventoSingolo.as_view(), name='evento-singolo'),
    path('event/user_event', views.event_user, name='event-user'),
    path('event/<int:pk>/delete', views.DeleteEvent.as_view(), name='event-delete'),
    path('event/user_delete_event', views.elimina_evento_user, name='event-delete-user'),
    path('event/<int:pk>/delete_user_evento', views.DeleteUserFromEvent.as_view(), name='event-delete-user-evento'),
    path('event/<int:pk>/seminario/update', views.SeminarioUpdate.as_view(), name='seminario-update'),
    path('event/<int:pk>/corso/update', views.CorsoUpdate.as_view(), name='corso-update'),
    path('event/<int:pk>/competizione/update', views.CompetizioneUpdate.as_view(), name='competizione-update'),
    path('event/<int:pk>/sportivo/update', views.SportivoUpdate.as_view(), name='sportivo-update'),
    path('event/<int:pk>/list_user', views.PartecipantiEvento.as_view(), name='event-list'),
]
