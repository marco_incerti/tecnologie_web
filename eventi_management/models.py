from django.conf import settings
from django.db import models

# Create your models here.
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


class Universita(models.Model):
    name = models.CharField(max_length=150)
    citta = models.CharField(max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


class Corso_di_laurea(models.Model):
    name = models.CharField(max_length=150)
    universita = models.ForeignKey(Universita, on_delete=models.CASCADE)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


class Tipo_festa(models.Model):
    name = models.CharField(max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


class Tipo_tutorato(models.Model):
    name = models.CharField(max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


# Create your models here.
class Tags(models.Model):
    name = models.CharField(max_length=150)

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['id']


class Event(models.Model):
    user_owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='user_events')  # Reference to the table User, created automatically by
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True,
                                 related_name='category_event')
    title = models.CharField(max_length=150)
    descrizione = models.CharField(max_length=15000)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField(null=True)
    date_create = models.DateTimeField(auto_now_add=True)
    posti = models.IntegerField(null=True, blank=True)
    foto = models.ImageField(upload_to='images/', null=True, blank=True)
    luogo = models.CharField(max_length=50, null=True, blank=True)

    long = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)

    email_extra = models.EmailField(null=True, blank=True)
    link = models.URLField(max_length=250, null=True, blank=True)
    evento_online = models.BooleanField(default=False)

    gratuito = models.BooleanField(default=True)
    prezzo = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)

    universita = models.ForeignKey(Universita, on_delete=models.SET_NULL, null=True, blank=True)

    def get_absolute_url(self):
        return reverse('home_user')

    class Meta:
        ordering = ['date_start']

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class Seminario(Event):
    ospite = models.CharField(max_length=250, null=True, blank=True)


class Corso(Event):
    responsabile = models.CharField(max_length=250, null=True, blank=True)


class Competizione(Event):
    responsabile = models.CharField(max_length=250, null=True, blank=True)
    sponsor = models.CharField(max_length=250, null=True, blank=True)
    n_persone_per_squadra = models.IntegerField(null=True, blank=True)
    premio = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


class Sportivo(Event):
    responsabile = models.CharField(max_length=250, null=True, blank=True)
    sport = models.CharField(max_length=250, null=True, blank=True)


class Festa(Event):
    tipo_festa = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True,
                                   related_name='tipo_festa_event')
    genere_musicale = models.CharField(max_length=250, null=True, blank=True)


class Tutorato(Event):
    tipo_tutorato = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True,
                                      related_name='tipo_tutorato_event')
    materia = models.CharField(max_length=250, null=True, blank=True)
    anno = models.IntegerField(null=True, blank=True)
