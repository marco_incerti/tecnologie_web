import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit
from django import forms

from eventi_management.models import Seminario, Category, Corso, Sportivo, Competizione
from eventi_management.widgets import BootstrapDateTimePickerInput


class EventInsertForm(forms.ModelForm):
    helper = FormHelper()
    helper.add_input(Submit('submit', 'SALVA', css_class='btn btn-info'))

    title = forms.CharField(max_length=150, label='Titolo:')
    descrizione = forms.CharField(max_length=15000, widget=forms.Textarea, label='Descrizione:')
    date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=BootstrapDateTimePickerInput())
    category = forms.ModelChoiceField(queryset=Category.objects.all(), empty_label='Nessuna categoria',
                                      label='Categoria')
    posti = forms.IntegerField(label='Posti disponibili (se non è presente un massimo lasciare vuoto):', min_value=0,
                               required=False)
    foto = forms.ImageField(label='Foto evento:', required=False)
    luogo = forms.CharField(max_length=150, label='Luogo:', required=False)
    link = forms.URLField(label='Link:', max_length=250, required=False)
    email_extra = forms.EmailField(label='Email extra:', required=False)

    def clean(self):
        date = self.cleaned_data.get("date")
        if date.date() < datetime.date.today():
            raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
        return self.cleaned_data

    # def clean_start_date(self):
    #     st_date = self.cleaned_data.get("date")
    #
    #     if st_date < datetime.date.today():
    #         raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
    #     return st_date


class SeminarioInsertForm(EventInsertForm):
    ospite = forms.CharField(max_length=150, label='Ospite:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-6 mb-0'),
                Column('ospite', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    class Meta:
        model = Seminario
        fields = ('title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'ospite', 'email_extra')


class CorsoInsertForm(EventInsertForm):
    responsabile = forms.CharField(max_length=150, label='Responsabile:', required=False)
    prezzo = forms.DecimalField(max_digits=10, label='Prezzo:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('prezzo', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    class Meta:
        model = Corso
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'prezzo')


class CompetizioneInsertForm(EventInsertForm):
    responsabile = forms.CharField(max_length=150, label='Responsabile:', required=False)
    sponsor = forms.CharField(max_length=150, label='Sponsor:', required=False)
    n_persone_per_squadra = forms.CharField(max_length=150, label='Numero di persone per squadra:')
    premio = forms.DecimalField(label='Premio:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('premio', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('sponsor', css_class='form-group col-md-6 mb-0'),
                Column('n_persone_per_squadra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    class Meta:
        model = Competizione
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'premio', 'n_persone_per_squadra', 'sponsor')


class SportivoInsertForm(EventInsertForm):
    responsabile = forms.CharField(max_length=150, label='Responsabile:', required=False)
    sport = forms.CharField(max_length=150, label='Sport:', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('sport', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    class Meta:
        model = Sportivo
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'sport')


class UpdateSeminarioCrispyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'update-seminario-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva', css_class='btn btn-success'))
    helper.add_input(Submit('cancel', 'Annulla', css_class='btn btn-secondary'))

    class Meta:
        model = Seminario
        fields = ('title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'ospite', 'email_extra')
        widgets = {'descrizione': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('email_extra', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-6 mb-0'),
                Column('ospite', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-3 mb-0'),
                Column('foto', css_class='form-group col-md-6 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date.date() < datetime.date.today():
            raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
        return date


class UpdateCorsoCrispyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'update-corso-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva', css_class='btn btn-success'))
    helper.add_input(Submit('cancel', 'Annulla', css_class='btn btn-secondary'))

    class Meta:
        model = Corso
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'prezzo')
        widgets = {'descrizione': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('prezzo', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date.date() < datetime.date.today():
            raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
        return date


class UpdateCompetizioneCrispyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'update-competizione-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva', css_class='btn btn-success'))
    helper.add_input(Submit('cancel', 'Annulla', css_class='btn btn-secondary'))

    class Meta:
        model = Competizione
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'premio', 'n_persone_per_squadra', 'sponsor')
        widgets = {'descrizione': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('premio', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('sponsor', css_class='form-group col-md-6 mb-0'),
                Column('n_persone_per_squadra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date.date() < datetime.date.today():
            raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
        return date


class UpdateSportivoCrispyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'update-sportivo-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva', css_class='btn btn-success'))
    helper.add_input(Submit('cancel', 'Annulla', css_class='btn btn-secondary'))

    class Meta:
        model = Sportivo
        fields = (
            'title', 'date_start', 'descrizione', 'posti', 'foto', 'luogo', 'category', 'link', 'responsabile', 'email_extra',
            'sport')
        widgets = {'descrizione': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-9 mb-0'),
                Column('date', css_class='form-row'),
                css_class='form-row'
            ),
            Row(
                Column('descrizione', css_class='form-group mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('link', css_class='form-group col-md-6 mb-0'),
                Column('email_extra', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('luogo', css_class='form-group col-md-3 mb-0'),
                Column('responsabile', css_class='form-group col-md-6 mb-0'),
                Column('sport', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('posti', css_class='form-group col-md-6 mb-0'),
                Column('foto', css_class='form-group col-md-3 mb-0'),
                Column('category', css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date.date() < datetime.date.today():
            raise forms.ValidationError("Non puoi inserire date precedenti ad oggi!")
        return date
