from django.contrib import admin

# Register your models here.
from eventi_management.models import Seminario, Category, Tags, Event, Corso, Competizione, Sportivo

admin.site.register(Seminario)
admin.site.register(Event)
admin.site.register(Corso)
admin.site.register(Competizione)
admin.site.register(Sportivo)
admin.site.register(Category)
admin.site.register(Tags)
