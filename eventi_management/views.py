from datetime import date
from itertools import chain

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, TemplateView, UpdateView, DeleteView

from DAWEY.views import BasicView
from eventi_management.forms import SeminarioInsertForm, UpdateSeminarioCrispyForm, CorsoInsertForm, \
    CompetizioneInsertForm, \
    SportivoInsertForm, UpdateCorsoCrispyForm, UpdateCompetizioneCrispyForm, UpdateSportivoCrispyForm
from eventi_management.models import Seminario, Corso, Competizione, Sportivo, Event
from user_management.models import UserAndEvent


class InsertEvent(TemplateView):
    template_name = 'inserimeto_evento/inserimento_evento_generico.html'


class SeminarioInsert(LoginRequiredMixin,CreateView):
    template_name = 'inserimeto_evento/inserimento_form.html'
    form_class = SeminarioInsertForm
    model = Seminario

    def form_valid(self, form):
        if form.is_valid():
            evento = form.save(commit=False)
            evento.user_owner = self.request.user
            evento.save()
        return super().form_valid(form)


class CompetizioneEvent(LoginRequiredMixin,CreateView):
    template_name = 'inserimeto_evento/inserimento_form.html'
    form_class = CompetizioneInsertForm
    model = Competizione

    def form_valid(self, form):
        if form.is_valid():
            evento = form.save(commit=False)
            evento.user_owner = self.request.user
            evento.save()
        return super().form_valid(form)


class CorsoInsert(LoginRequiredMixin,CreateView):
    template_name = 'inserimeto_evento/inserimento_form.html'
    form_class = CorsoInsertForm
    model = Corso

    def form_valid(self, form):
        if form.is_valid():
            evento = form.save(commit=False)
            evento.user_owner = self.request.user
            evento.save()
        return super().form_valid(form)


class SportivoInsert(LoginRequiredMixin,CreateView):
    template_name = 'inserimeto_evento/inserimento_form.html'
    form_class = SportivoInsertForm
    model = Sportivo

    def form_valid(self, form):
        if form.is_valid():
            evento = form.save(commit=False)
            evento.user_owner = self.request.user
            evento.save()
        return super().form_valid(form)


class EventListUserCreate(LoginRequiredMixin, BasicView, TemplateView):
    template_name = 'user_list_created_event.html'

    tutti = None
    seminario = None
    corso = None
    competizione = None
    sportivo = None
    grafico = None

    def get_context_data(self, **kwargs):
        context = super(EventListUserCreate, self).get_context_data(**kwargs)

        self.seminario = Seminario.objects.filter(user_owner_id=self.request.user.pk)
        self.corso = Corso.objects.filter(user_owner_id=self.request.user.pk)
        self.competizione = Competizione.objects.filter(user_owner_id=self.request.user.pk)
        self.sportivo = Sportivo.objects.filter(user_owner_id=self.request.user.pk)
        self.grafico = Event.objects.filter(user_owner_id=self.request.user.pk)
        self.tutti = chain(self.seminario, self.corso, self.competizione, self.sportivo)
        return context


class EventoSingolo(LoginRequiredMixin, BasicView, TemplateView):
    template_name = 'evento_singolo.html'
    evento = None
    creatore = None

    def get_context_data(self, **kwargs):
        context = super(EventoSingolo, self).get_context_data(**kwargs)
        self.evento = Event.objects.get(pk=self.kwargs['pk'])
        self.creatore = User.objects.get(pk=self.evento.user_owner_id)
        if self.request.user.is_authenticated:
            id_eventi_prenotati = UserAndEvent.objects.filter(user=self.request.user).values('id_event')
            self.object_eventi_prenotati = Event.objects.filter(date__gte=date.today(),
                                                                pk__in=id_eventi_prenotati)
        return context


class PartecipantiEvento(LoginRequiredMixin, BasicView, TemplateView):
    template_name = 'list_user_event.html'
    partecipanti = None

    def get_context_data(self, **kwargs):
        context = super(PartecipantiEvento, self).get_context_data(**kwargs)

        id_utenti_prenotati = UserAndEvent.objects.filter(id_event=self.kwargs['pk']).values('user')
        self.partecipanti = User.objects.filter(pk__in=id_utenti_prenotati)
        return context


class EventoUpdate(LoginRequiredMixin, BasicView, UpdateView):
    template_name = 'updateEvent.html'

    def post(self, request, *args, **kwargs):
        """Add 'Cancel' button redirect."""
        if "cancel" in request.POST:
            # url = reverse('text_management:text-list')
            url = reverse_lazy('eventi_management:event-list')
            return HttpResponseRedirect(url)
        else:
            return super(EventoUpdate, self).post(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        """Checks if user authenticated is owner of the event"""
        if request.user != self.get_object().user_owner:
            # Block requests that attempt to provide their own foo value
            return HttpResponseForbidden()
        # now process dispatch as it otherwise normally would
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('eventi_management:event-list')


class SeminarioUpdate(EventoUpdate):
    model = Seminario
    form_class = UpdateSeminarioCrispyForm


class CorsoUpdate(EventoUpdate):
    model = Corso
    form_class = UpdateCorsoCrispyForm


class CompetizioneUpdate(EventoUpdate):
    model = Competizione
    form_class = UpdateCompetizioneCrispyForm


class SportivoUpdate(EventoUpdate):
    model = Sportivo
    form_class = UpdateSportivoCrispyForm


class DeleteUserFromEvent(BasicView, DeleteView):
    model = UserAndEvent
    template_name = 'delete_user_from_event.html'
    utente = None
    success_url = reverse_lazy('eventi_management:event-list')

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class DeleteEvent(BasicView, DeleteView):
    model = Event
    template_name = 'event_delete.html'
    success_url = reverse_lazy('eventi_management:event-list')

    def dispatch(self, request, *args, **kwargs):
        """Checks if user authenticated is owner of the text analysis"""
        if request.user != self.get_object().user_owner:
            # Block requests that attempt to provide their own foo value
            return HttpResponseForbidden()
        # now process dispatch as it otherwise normally would
        return super().dispatch(request, *args, **kwargs)


@login_required
@require_POST
@csrf_protect
def event_user(request):
    if request.method == "POST":
        evento_prov = Event.objects.get(pk=request.POST.get('event_id'))
        user_prov = User.objects.get(pk=request.POST.get('user_id'))
        user_and_event = UserAndEvent(user=user_prov, id_event=evento_prov)
        user_and_event.save()

        data = {
            'text_saved': True
        }
        return JsonResponse(data)


@login_required
@require_POST
@csrf_protect
def elimina_evento_user(request):
    if request.method == "POST":
        UserAndEvent.objects.filter(user=request.POST.get('user_id'),
                                    id_event=request.POST.get('event_id')).delete()
        data = {
            'text_saved': True
        }
        return JsonResponse(data)
